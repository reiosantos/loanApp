
export const environment = {
  production: false,
  currentUser: "currentLendersUser",
  currentUserID: "currentLendersUserID",
  firebase : {
    apiKey: "AIzaSyDWtHn53CDDgT_UtgksLvrxUDRh42zhtgo",
    authDomain: "lenders-dff1b.firebaseapp.com",
    databaseURL: "https://lenders-dff1b.firebaseio.com",
    projectId: "lenders-dff1b",
    storageBucket: "lenders-dff1b.appspot.com",
    messagingSenderId: "594772269152",
  },
  generateUniqueId: function () {
    const d = new Date();
    return d.getFullYear().toString()+ d.getMonth().toString() + d.getDate().toString() + d.getHours().toString() +
      d.getMinutes().toString() + d.getSeconds().toString();
  },
  generateDate: function () {
    const d = new Date();
    return (d.getFullYear().toString() + "-" +
      ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)).toString() + "-" +
      (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()).toString() + " " +
      (d.getHours() < 10 ? '0' + d.getHours() : d.getHours()).toString() + ":" +
      (d.getMinutes() < 10 ?'0' + d.getMinutes() : d.getMinutes()).toString() + ":" +
      (d.getSeconds() < 10 ? '0' + d.getSeconds():  d.getSeconds()).toString());
  }
};

