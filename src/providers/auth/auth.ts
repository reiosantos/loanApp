import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {AlertService} from "../util/alert.service";
import {ToastService} from "../util/toast.service";
import {App} from "ionic-angular";
import {UserModel} from "../../interaces/user.model";
import {environment} from "../../environments/environment";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  public user: UserModel;
  constructor(
    public app: App,
    public alertService: AlertService,
    public toastCtrl: ToastService,
  ) { }

  async loginUser(email: string, password: string): Promise<any> {
    let sat = await
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(newUser=>{
        localStorage.setItem(environment.currentUserID, newUser.uid);
        localStorage.setItem('loan_email', newUser.email);

        return this.fetchUser(newUser);
      });
    return sat;
  }

  async signupUser(email: string, password: string): Promise<any> {
    let tes = await
      firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then( (newUser) => {
        localStorage.setItem(environment.currentUserID, newUser.uid);
        localStorage.setItem('loan_email', newUser.email);

        firebase.database().ref('/userProfile').child(newUser.uid).child("email").set(email).then(
          () => {
            return this.fetchUser(newUser);
          }, () => {  return false; }
        );
      });
    return tes
  }

  fetchUser(us){
    return firebase.database().ref('/userProfile').child(us.uid).on('value', (snapshot) => {
      this.user = snapshot.exportVal();
      this.user.email = us.email;

      localStorage.setItem(environment.currentUser, JSON.stringify(this.user));
      }, function (error) {
      console.log(error);
    });
  }

  static resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  logOut() {
    this.alertService.presentAlertWithCallback('Are you sure?',
      'This will log you out of this application.').then((yes) => {
      if (yes) {
        // noinspection JSIgnoredPromiseFromCall
        firebase.auth().signOut();
        localStorage.clear();
        const root = this.app.getRootNav();
        // noinspection JSIgnoredPromiseFromCall
        root.setRoot('LoginPage');
        this.toastCtrl.create('Logged out of the application');
      }
    });
  }
}
