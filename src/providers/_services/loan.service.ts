import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import {LoanModel} from "../../interaces/loan.model";
import {PaymentModel} from "../../interaces/payment.model";

@Injectable()
export class LoanService {
  constructor() { }

  public static saveNewLoan(loan: LoanModel, uid: any): Promise<any> {
    return firebase.database().ref('/loans').child(uid).child(loan.loan_id).set(loan);
  }

  public static updateLoanStatus(loan: LoanModel, uid: any): Promise<any> {
    if (loan.status === "cleared"){
      loan.isCleared = true;
    }
    return firebase.database().ref('/loans').child(uid).child(loan.loan_id).update(
      { "status": loan.status, "isCleared": loan.isCleared });
  }

  public static addPayment(loan: LoanModel, uid: any, payment: PaymentModel): Promise<any> {

    loan.paid = parseFloat(loan.paid) + payment.amount;
    loan.balance = parseFloat(loan.total) - loan.paid;
    if (loan.balance <= 0){
      loan.status = "cleared";
      loan.isCleared = true;
    }
    return firebase.database().ref('/loans').child(uid).child(loan.loan_id).set(loan);
  }

  public static getUnpaidLoans(uid: any): firebase.database.Query {
    return firebase.database().ref('/loans').child(uid).orderByChild("isCleared")
      .equalTo(false);
  }

  public static getAllLoans(uid: any): firebase.database.Query {
    return firebase.database().ref('/loans').child(uid).orderByChild("date");
  }

  public static getAllDebtors(): firebase.database.Query {
    return firebase.database().ref('/loans').orderByKey();
  }


  /*getAll() : boolean{
      return false;
  }

  createOrUpdate():boolean {                                                                                                                                ```````````````````````````````````````````````````                                                   `
      return false;
  }

  delete(id: number) {
      // return this.http.get<any>(environment.api + '?action=delete&table=users&id=' + id);
  }*/
}
