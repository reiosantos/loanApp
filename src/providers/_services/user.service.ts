﻿import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import {UserModel} from "../../interaces/user.model";

@Injectable()
export class UserService {
    constructor() { }

    public static saveProfile(user: UserModel, uid: any): Promise<any> {
      return firebase.database().ref('/userProfile').child(uid).set(user);
    }

    public static lenders(): firebase.database.Query {
      return firebase.database().ref('/userProfile').orderByChild("userType").equalTo("lender");
    }

    /*getAll() : boolean{
        return false;
    }

    createOrUpdate():boolean {                                                                                                                                ```````````````````````````````````````````````````                                                   `
        return false;
    }

    delete(id: number) {
		// return this.http.get<any>(environment.api + '?action=delete&table=users&id=' + id);
    }*/
}
