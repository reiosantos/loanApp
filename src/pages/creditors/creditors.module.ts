import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreditorsPage } from './creditors';

@NgModule({
  declarations: [
    CreditorsPage,
  ],
  imports: [
    IonicPageModule.forChild(CreditorsPage),
  ],
  exports: [
    CreditorsPage,
  ],
})
export class CreditorsPageModule {}
