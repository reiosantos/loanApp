import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {UserModel} from "../../interaces/user.model";
import {UserService} from "../../providers/_services/user.service";
import {isUndefined} from "ionic-angular/util/util";

/**
 * Generated class for the CreditorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-creditors',
  templateUrl: 'creditors.html',
})
export class CreditorsPage {

  lenders: UserModel[] = [];
  loading: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams
  ) {

    this.getCreditors();
  }

  getCreditors() {

    let done = false;
    UserService.lenders().on('value',

      (snapshot) => {

        done = true;
        if(!snapshot.exists() || isUndefined(snapshot)){
          if(this.loading) { this.loading.dismiss(); }
          return true;
        }
        if(this.loading) { this.loading.dismiss(); }
        let temp: UserModel[] = [];
        if (snapshot.numChildren() > 0){

          let data = snapshot.val();
          let keys = Object.keys(data);
          for(let key in keys){

            let hash = keys[key];
            let use: UserModel = data[hash];
            temp.push(use);
          }
          /*snapshot.forEach(snap => {
            let use: UserModel = snap.val();
            temp.push(use);
            //return true;
          });*/
        }
        this.lenders = temp;
      },
      (error) => {
        done = true;
        if(this.loading) { this.loading.dismiss(); }
      });

      setTimeout(() => {
        if (!done){
          this.loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: "Error: Slow network is detected. Please refresh later.",
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }
      }, 30000);

    this.loading = this.loadingCtrl.create({ content: 'loading....'});
    if (this.lenders.length < 1){ this.loading.present() };

  }

  refresh(){
    location.reload();
  }

}
