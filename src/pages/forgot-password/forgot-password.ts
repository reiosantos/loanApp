import { Component } from '@angular/core';
import {
  AlertController, App, IonicPage, MenuController, NavController,
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EmailValidator} from "../../validators/email";

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  public resetPasswordForm: FormGroup;
  public backgroundImage = 'assets/imgs/background/background-6.jpg';

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public app: App,
    public formBuilder: FormBuilder,
    public menu: MenuController,
  ) {
    this.resetPasswordForm = formBuilder.group({
      email: ['',
        Validators.compose([Validators.required, EmailValidator.isValid])],
    });
  }

  ionViewDidEnter(): void {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

  goToLogin() {
    // noinspection JSIgnoredPromiseFromCall
    this.navCtrl.popTo('LoginPage');
  }

  resetPassword(){
    if (!this.resetPasswordForm.valid){
      console.log(this.resetPasswordForm.value);
    } else {
      AuthProvider.resetPassword(this.resetPasswordForm.value.email)
        .then(() => {
          let alert = this.alertCtrl.create({
            message: "We sent you a reset link to your email",
            buttons: [
              {
                text: "Ok",
                role: 'cancel',
                handler: () => { // noinspection JSIgnoredPromiseFromCall
                  this.navCtrl.popTo('LoginPage'); }
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();

        }, (error) => {
          const errorMessage: string = error.message;
          let errorAlert = this.alertCtrl.create({
            message: errorMessage,
            buttons: [{ text: "Ok", role: 'cancel' }]
          });
          // noinspection JSIgnoredPromiseFromCall
          errorAlert.present();
        });
    }
  }

}
