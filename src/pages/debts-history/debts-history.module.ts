import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DebtsHistoryPage } from './debts-history';

@NgModule({
  declarations: [
    DebtsHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(DebtsHistoryPage),
  ],
})
export class DebtsHistoryPageModule {}
