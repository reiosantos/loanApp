import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoanModel} from "../../interaces/loan.model";

/**
 * Generated class for the DebtsHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-debts-history',
  templateUrl: 'debts-history.html',
})
export class DebtsHistoryPage {

  debts: LoanModel[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    if(this.navParams.get('debts') !== undefined){
      this.debts = this.navParams.get('debts')
    }

  }

  goBack(){
    this.navCtrl.pop();
  }

}
