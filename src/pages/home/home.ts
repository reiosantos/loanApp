import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, MenuController, NavController} from 'ionic-angular';
import {LoanService} from "../../providers/_services/loan.service";
import {environment} from "../../environments/environment";
import {UserModel} from "../../interaces/user.model";
import {LoanModel} from "../../interaces/loan.model";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  uid: any;
  user: UserModel;

  images: any = ['money4.jpeg', 'money5.jpeg', 'money3.jpeg', 'money6.jpeg', 'money2.jpeg', 'money1.jpeg'];
  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    ) {

    if (localStorage.getItem(environment.currentUserID) !== null){
      this.uid = localStorage.getItem(environment.currentUserID);
    }
    if (localStorage.getItem(environment.currentUser) !== null){
      this.user = JSON.parse(localStorage.getItem(environment.currentUser));
    }// else{ location.assign('login'); }

    this.checkLoanStatus();
  }

  checkLoanStatus(){
    let done = false;

    let loading = this.loadingCtrl.create({content: 'Checking your loan eligibility..wait'});
    // noinspection JSIgnoredPromiseFromCall
    loading.present();

    LoanService.getUnpaidLoans(this.uid).on('value',

      (snapshot) => {
        done = true;

        if (loading) { loading.dismiss(); }

        if (snapshot.numChildren() > 0){
          let count = 0;
          snapshot.forEach(snap => {
            let use: LoanModel = snap.val();

            if (use.isCleared !== true && use.status === "accepted" && count < 1) {
              let alert1 = this.alertCtrl.create({
                message: 'You still have a loan debt. You cant request for a new loan..',
                buttons: [
                  {
                    text: "close",
                    role: 'cancel'
                  }
                ]
              });
              // noinspection JSIgnoredPromiseFromCall
              alert1.present();
              count++;
            }
            return true;
          });
        }
      },
      (error) => {
        done = true;
        console.log(error);
        if (loading) { loading.dismiss(); }
      }
    );

    setTimeout(() => {
      if (!done){
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);

    // noinspection JSIgnoredPromiseFromCall

  }

}
