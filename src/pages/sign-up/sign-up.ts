import { Component } from '@angular/core';
import {AlertController, App, IonicPage, LoadingController, MenuController, NavController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EmailValidator} from "../../validators/email";

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  public signupForm: FormGroup;
  public backgroundImage = 'assets/imgs/background/background-6.jpg';
  public loading: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public authProvider: AuthProvider,
    public formBuilder: FormBuilder,
    public menu: MenuController,
  ) {
    this.signupForm = formBuilder.group({
      email: ['',
        Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['',
        Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

  goToLogin() {
    this.navCtrl.popTo('LoginPage');
  }

  signupUser(){

    if (!this.signupForm.valid){
      console.log(this.signupForm.value);
    } else {
      this.authProvider.signupUser(this.signupForm.value.email,
        this.signupForm.value.password)
        .then(() => {

          this.loading.dismiss().then( () => {
            this.navCtrl.setRoot('ProfilePage', {'new_setup': true, });
          });

        }, (error) => {

          this.loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();

          });
        });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

}
