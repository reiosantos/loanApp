import {Component, OnInit} from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, LoadingController,
  NavController, NavParams,
  Platform
} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../providers/_services/user.service";
import {UserModel} from "../../interaces/user.model";
import {isUndefined} from "ionic-angular/util/util";
import {LoanService} from "../../providers/_services/loan.service";
import {LoanModel} from "../../interaces/loan.model";
import {environment} from "../../environments/environment";
import {CameraProvider} from "../../providers/util/camera.provider";
import {FileProvider} from "../../providers/util/file.provider";

/**
 * Generated class for the NewLoanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-loan',
  templateUrl: 'new-loan.html',
})
export class NewLoanPage implements OnInit {

  actionSheet : any;
  loanForm: FormGroup;
  submitted = false;
  loading: any;
  security_name: any;
  private security_file: any = null;
  uid: any;
  user: UserModel;
  loan: LoanModel;
  lenders: UserModel[] = [];
  securities: any = [];
  hasLoan = false;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private cameraProvider: CameraProvider,
    private fileProvider: FileProvider
  ) {

    this.loanForm = formBuilder.group({
      amount: ['', Validators.compose([Validators.required, Validators.min(50)])],
      rate: ['', Validators.compose([Validators.required, Validators.min(0)])],
      lender: ['', Validators.compose([Validators.required])],
      security: ['', ],
    });
    if (localStorage.getItem(environment.currentUserID) !== null){
      this.uid = localStorage.getItem(environment.currentUserID);
    }
    if (localStorage.getItem(environment.currentUser) !== null){
      this.user = JSON.parse(localStorage.getItem(environment.currentUser));
    }
    this.loan = new LoanModel();
  }

  ngOnInit() {
    this.checkLoanStatus();
  }

  checkLoanStatus(){
    let done = false;

    let loading = this.loadingCtrl.create({content: 'Checking your loan eligibility..wait'});
    // noinspection JSIgnoredPromiseFromCall
    loading.present();

    LoanService.getUnpaidLoans(this.uid).on('value',

      (snapshot) => {
        done = true;

        if (loading) { loading.dismiss(); }

        if (snapshot.numChildren() > 0){
          let count = 0;
          snapshot.forEach(snap => {
            let use: LoanModel = snap.val();

            if (use.isCleared !== true && use.status === "accepted" && count < 1) {
              this.hasLoan = true;
              count++;
            }
            return true;
          });
        }
        this.getLenders();
      },
      (error) => {
        done = true;
        console.log(error);
        if (loading) { loading.dismiss(); }
      }
    );

    setTimeout(() => {
      if (!done){
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);

    // noinspection JSIgnoredPromiseFromCall

  }

  getLenders() {
    let done = false;
    this.loading = this.loadingCtrl.create({ content: 'Loading lenders list...Please wait.'});
    if (this.lenders.length < 1){ this.loading.present(); }

    UserService.lenders().on('value',

      (snapshot) => {
        let temp: UserModel[] = [];
        done = true;

        if (snapshot.numChildren() > 0){
          // noinspection TypeScriptValidateTypes
          let data = snapshot.val();
          let keys = Object.keys(data);
          for(let key in keys){
            let hash = keys[key];
            let use: UserModel = data[hash];
            temp.push(use);
          }
          /*snapshot.forEach(snap => {
            let use: UserModel = snap.val();
            temp.push(use);
            //return true;
          });*/
        }
        this.lenders = temp;
        if (this.loading) { this.loading.dismiss(); }
      },
      (error) => {
      done = true;
        if (this.loading) { this.loading.dismiss(); }
      }
    );

    setTimeout(() => {
      if (!done){
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);

  }

  /*
  private generateFileName(loan_ID: string): string {
    return this.user.email + loan_ID;
  }
  */

  onLenderChange(){
    for(let i = 0; i < this.lenders.length; i++){
      let lender = this.lenders[i];
      if(lender.accountNo === this.loan.lender){
        this.loan.interestRate = lender.interestRate;
        this.loan.lender_name = lender.company;
        this.securities = lender.securities;
        return false;
      }
    }
  }

  applyLoan() {

    if(this.hasLoan){
      let alert1 = this.alertCtrl.create({
        message: 'You now have a loan debt. You cant request for a new loan.. First pay the debt please',
        buttons: [
          {
            text: "close",
            role: 'cancel'
          }
        ]
      });
      // noinspection JSIgnoredPromiseFromCall
      alert1.present();
      return false;
    }
    this.submitted = true;

    if (this.loanForm.valid) {
      if(isUndefined(this.uid)) {
        let alert = this.alertCtrl.create({
          message: 'Please logout and in again to continue. Phone storage has been corrupted',
          buttons: [
            {
              text: "close",
              role: 'cancel'
            }
          ]
        });
        // noinspection JSIgnoredPromiseFromCall
        alert.present();
        return false;
      }

      let done = false;
      let loading = this.loadingCtrl.create({content: "saving loan..."});
      loading.present();

      this.loan.loan_id = environment.generateUniqueId();
      this.security_name = this.loanForm.controls.security.value; // this.generateFileName(this.loan.loan_id);
      this.loan.security_name = isUndefined(this.security_name) ? '' : this.security_name;
      this.loan.clientUid = this.uid;
      this.loan.date = environment.generateDate();

      this.loan.amount = parseFloat(this.loanForm.controls.amount.value);
      this.loan.interestRate = parseFloat(this.loanForm.controls.rate.value);
      this.loan.lender = this.loanForm.controls.lender.value;
      this.loan.interest = (this.loan.amount * this.loan.interestRate) / 100;
      this.loan.status = 'pending';
      this.loan.isCleared = false;
      this.loan.total = (this.loan.interest + this.loan.amount);
      this.loan.paid = 0;
      this.loan.balance = (this.loan.total - this.loan.paid);
      this.loan.clientName = (this.user.fullName);

      // to submit loan and security objects
      LoanService.saveNewLoan(this.loan, this.uid).then(
        () => {
          done = true;
          this.hasLoan = true;

          loading.dismiss().then( () => {

            let alert = this.alertCtrl.create({
              message: 'Loan Has been saved... We shall be notifying you when the loan is approved. Keep checking.',
              buttons: [
                {
                  text: "Ok", role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }, error => {
          done = true;
          loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok", role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }
      );

      setTimeout(() => {
        if (!done){
          loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: "Error: Slow network is detected. Please refresh later.",
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }
      }, 30000);

    }
  }

  attachFile() {

    this.actionSheet = this.actionSheetCtrl.create({
      title: "Upload File",
      buttons: [
        {
          text: "Photo", role: "destructive", handler: () => {
            let ac = this.actionSheetCtrl.create({
              title: "Upload Photo",
              buttons: [
                {
                  text: "Camera", role: "destructive", icon: "camera", handler: () => {
                    this.security_file = this.cameraProvider.getPictureFromCamera();
                  }
                },
                {
                  text: "Gallery", role: "destructive", icon: "gallery", handler: () => {
                    this.security_file = this.cameraProvider.getPictureFromPhotoLibrary()
                  }
                },
                {
                  text: "Cancel", role: "cancel", handler: () => {console.log('canceled sheet 2')}
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            ac.present();
          }
        },
        {
          text: "Document", role: "destructive", handler: () => {
            this.security_file = this.fileProvider.getFileFromLibrary();
          }
        },
        {
          text: "Cancel", role: "cancel", handler: () => {console.log('canceled sheet')}
        }
      ]
    });
    this.actionSheet.present();
  }

}
