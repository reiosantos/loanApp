import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewLoanPage } from './new-loan';

@NgModule({
  declarations: [
    NewLoanPage,
  ],
  imports: [
    IonicPageModule.forChild(NewLoanPage),
  ],
  exports: [
    NewLoanPage,
  ],
})
export class NewLoanPageModule {}
