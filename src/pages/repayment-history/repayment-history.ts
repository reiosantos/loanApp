import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PaymentModel} from "../../interaces/payment.model";

/**
 * Generated class for the RepaymentHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'RepaymentHistoryPage'
})
@Component({
  selector: 'page-repayment-history',
  templateUrl: 'repayment-history.html',
})
export class RepaymentHistoryPage {

  payments: PaymentModel[] = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    if(this.navParams.get('payments') !== undefined){
      this.payments = this.navParams.get('payments');
    }
  }

  goBack(){
    this.navCtrl.pop();
  }

}
