import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {LoanModel} from "../../interaces/loan.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoanService} from "../../providers/_services/loan.service";
import {environment} from "../../environments/environment";
import {PaymentModel} from "../../interaces/payment.model";
import {isArray} from "ionic-angular/util/util";

@IonicPage({
  name: 'AddPaymentPage'
})
@Component({
  selector: 'page-add-payment',
  templateUrl: 'add-payment.html',
})
export class AddPaymentPage {

  loan: LoanModel;
  paymentModel: PaymentModel;
  paymentForm: FormGroup;
  submitted = false;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public navParams: NavParams) {

    this.loan = new LoanModel();
    this.paymentModel = new PaymentModel();
    this.paymentModel.date = environment.generateDate();

    if(this.navParams.get('loan') !== undefined){
      this.loan = this.navParams.get('loan');
    }

    this.paymentForm = formBuilder.group({
      amount: ['', Validators.compose([Validators.required, Validators.min(50)])],
      payment_date: ['', Validators.compose([Validators.required])],
    });
  }

  goBack(){
    // noinspection JSIgnoredPromiseFromCall
    this.navCtrl.pop();
  }

  savePayment() {

    this.submitted = true;
    if (this.paymentForm.valid) {

      let done = false;
      let loading = this.loadingCtrl.create({content: "saving payment..."});
      // noinspection JSIgnoredPromiseFromCall
      loading.present();

      this.paymentModel.payment_id = environment.generateUniqueId();
      this.paymentModel.date = this.paymentForm.controls.payment_date.value.trim() !== "" ?
        this.paymentForm.controls.payment_date.value : environment.generateDate();

      this.paymentModel.amount = parseFloat(this.paymentForm.controls.amount.value);
      if (!isArray(this.loan.payments)){
        this.loan.payments = [];
      }
      this.loan.payments.push(this.paymentModel);

      // to submit loan and security objects
      LoanService.addPayment(this.loan, this.loan.clientUid, this.paymentModel).then(
        () => {
          done = true;

          loading.dismiss().then( () => {

            let alert = this.alertCtrl.create({
              message: 'Payment has been recorded.. Thank you..',
              buttons: [
                {
                  text: "Ok", role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }, error => {
          done = true;
          loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok", role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }
      );

      setTimeout(() => {
        if (!done){
          loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: "Error: No / Slow network is detected. Please refresh later.",
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            // noinspection JSIgnoredPromiseFromCall
            alert.present();
          });
        }
      }, 30000);

    }
  }

}
