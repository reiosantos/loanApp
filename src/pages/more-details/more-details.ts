import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoanModel} from "../../interaces/loan.model";
import {LoanService} from "../../providers/_services/loan.service";

/**
 * Generated class for the MoreDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-more-details',
  templateUrl: 'more-details.html',
})
export class MoreDetailsPage {

  isCreditor: boolean = false;
  debtor: LoanModel;
  status = [
    {key:'pending', name:'Pending'},
    {key:'accepted', name:'Accepted'},
    {key:'rejected', name:'Rejected'},
    {key:'cleared', name:'Cleared'},
  ];
  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {
    this.debtor = new LoanModel();

    this.isCreditor = this.navParams.get('isCreditor');
    this.debtor = this.navParams.get('data');

    this.form = formBuilder.group({
      status: ['', Validators.compose([Validators.required])],
    });

  }

  goBack(){
    this.navCtrl.pop();
  }

  save() {
    let done = false;
    let loading = this.loadingCtrl.create({content: "updating loan status...wait"});
    // noinspection JSIgnoredPromiseFromCall
    loading.present();

    LoanService.updateLoanStatus(this.debtor, this.debtor.clientUid).then(
      () => {
        done = true;

        loading.dismiss().then( () => {

          let alert = this.alertCtrl.create({
            message: 'Loan Has been updated... ',
            buttons: [
              {
                text: "Ok", role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }, error => {
        done = true;
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok", role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    );

    setTimeout(() => {
      if (!done){
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: No / Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);
    return;
  }

}
