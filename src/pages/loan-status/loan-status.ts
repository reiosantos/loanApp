import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {LoanModel} from "../../interaces/loan.model";
import {LoanService} from "../../providers/_services/loan.service";
import {environment} from "../../environments/environment";
import {UserModel} from "../../interaces/user.model";
import {isUndefined} from "ionic-angular/util/util";
import {MoreDetailsPage} from "../more-details/more-details";
import {AddPaymentPage} from "../add-payment/add-payment";
import {LoanHistoryPage} from "../loan-history/loan-history";
import {DebtsHistoryPage} from "../debts-history/debts-history";
import {RepaymentHistoryPage} from "../repayment-history/repayment-history";

/**
 * Generated class for the LoanStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loan-status',
  templateUrl: 'loan-status.html',
})
export class LoanStatusPage {

  unPainLoans: LoanModel[] = [];
  painLoans: LoanModel[] = [];
  clearedDebtors: LoanModel[] = [];
  unClearedDebtors: LoanModel[] = [];

  uid: any;
  user: UserModel;

  isCreditor: boolean = false;
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

    if (localStorage.getItem(environment.currentUserID) !== null){
      this.uid = localStorage.getItem(environment.currentUserID);
    }
    if (localStorage.getItem(environment.currentUser) !== null){
      this.user = JSON.parse(localStorage.getItem(environment.currentUser));
    }

    this.getLoans();

    if (this.user.userType === "lender") {
      this.isCreditor = true;
      this.getDebtors();
    }
  }

  getLoans() {

    let done = false;
    let loading = this.loadingCtrl.create({content: 'Loading....'});
    // noinspection JSIgnoredPromiseFromCall
    loading.present();

    LoanService.getAllLoans(this.uid).on('value',
      (snapshot) => {

        done = true;
        if(!snapshot.exists() || isUndefined(snapshot)){
          console.log("no loan made yet");
          if(loading) { loading.dismiss(); }
          return true;
        }
        if(loading) { loading.dismiss(); }

        let paid: LoanModel[] = [];
        let unpaid: LoanModel[] = [];

        if (snapshot.numChildren() > 0){
          snapshot.forEach(snap => {
            let use: LoanModel = snap.val();

            if (use.isCleared === true) {
              paid.push(use);
            } else {
              if(use.status === "accepted"){
                unpaid.push(use);
              }
            }
            return true;
          });
        }
        this.unPainLoans = unpaid;
        this.painLoans = paid;

      },error => {
        done = true;
        if(loading) {loading.dismiss(); }

      });

    setTimeout(() => {
      if (!done){
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);

  }

  getDebtors() {
    LoanService.getAllDebtors().on('value',
      (snapshot) => {

        if(!snapshot.exists() || isUndefined(snapshot)){
          console.log("no data found from server");
          return true;
        }

        let cleared: LoanModel[] = [];
        let unCleared: LoanModel[] = [];

        if (snapshot.numChildren() > 0){
          const data = snapshot.val();

          let keys = Object.keys(data);
          for (let i=0; i< keys.length; i++){
            let user = keys[i];
            let user_loans = data[user];
            /*if (user !== this.uid){
            }*/
            let loan_ids = Object.keys(user_loans);
            for(let j=0; j<loan_ids.length; j++){
              let loan: LoanModel = user_loans[loan_ids[j]];
              if (loan.lender === this.user.accountNo){
                if (loan.isCleared === true) {
                  cleared.push(loan);
                } else {
                  unCleared.push(loan);
                }
              }
            }
          }
        }
        this.clearedDebtors = cleared;
        this.unClearedDebtors = unCleared;

      },error => {
        console.log(error);

      });
  }

  moreDetails (loan: LoanModel){

    this.navCtrl.push(MoreDetailsPage, {
      'data': loan, 'isCreditor': this.isCreditor
    });

  }

  loanHistory(){
    this.navCtrl.push(LoanHistoryPage, {
      'loans': this.painLoans,
    });
  }

  debtsHistory(){
    this.navCtrl.push(DebtsHistoryPage, {
      'debts': this.clearedDebtors,
    });
  }

  acceptRequest(loan: LoanModel){

    let done = false;
    let loading = this.loadingCtrl.create({content: "updating loan status...wait"});
    // noinspection JSIgnoredPromiseFromCall
    loading.present();
    loan.status = "accepted";

    LoanService.updateLoanStatus(loan, loan.clientUid).then(
      () => {
        done = true;

        loading.dismiss().then( () => {

          let alert = this.alertCtrl.create({
            message: 'Loan Has been updated... ',
            buttons: [
              {
                text: "Ok", role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }, error => {
        done = true;
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok", role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    );

    setTimeout(() => {
      if (!done){
        loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Error: Slow network is detected. Please refresh later.",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          // noinspection JSIgnoredPromiseFromCall
          alert.present();
        });
      }
    }, 30000);
    return;
  }

  addPayment(loan: LoanModel){
    this.navCtrl.push(AddPaymentPage, {
      'loan': loan,
    });
  }

  repaymentHistory(loan: LoanModel){
    this.navCtrl.push(RepaymentHistoryPage, {
      'payments': loan.payments,
    });
  }

}
