import { Component } from '@angular/core';
import {IonicPage,NavController, NavParams} from 'ionic-angular';
import {LoanModel} from "../../interaces/loan.model";

/**
 * Generated class for the LoanHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'LoanHistoryPage'
})
@Component({
  selector: 'page-loan-history',
  templateUrl: 'loan-history.html',
})
export class LoanHistoryPage {

  loans: LoanModel[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    if(this.navParams.get('loans') !== undefined){
      this.loans = this.navParams.get('loans')
    }
  }

  goBack(){
    this.navCtrl.pop();
  }
}
