import {PaymentModel} from "./payment.model";

export class LoanModel{
  loan_id: any;
  date: any;
  amount: any;
  security_name: any;
  security_description: any;
  lender: any;
  lender_name: any;
  balance: any;
  status: any;
  total: any;
  paid: any;
  isCleared: boolean;
  interest: any;
  interestRate: any;
  clientUid: any;
  clientName: any;
  payments: PaymentModel[];
}
