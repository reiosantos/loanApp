
export interface UserModel {
  fullName: string;
  address: string;
  contact: string;
  country: string;
  userType: string;
  occupation: string;
  email: string;
  accountNo: any;
  company: string;
  company_location: string;
  securities: any;
  interestRate: any;
}
