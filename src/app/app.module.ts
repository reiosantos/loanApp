import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MainApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import {ToastService} from "../providers/util/toast.service";
import {AlertService} from "../providers/util/alert.service";
import {CameraProvider} from "../providers/util/camera.provider";
import {HttpClientModule} from "@angular/common/http";
import * as firebase from "firebase";
import {environment} from "../environments/environment";
import {Camera} from "@ionic-native/camera";
import {LoginPageModule} from "../pages/login/login.module";
import {HomePageModule} from "../pages/home/home.module";
import {UserService} from "../providers/_services/user.service";
import {LoanService} from "../providers/_services/loan.service";
import {FileProvider} from "../providers/util/file.provider";

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    MainApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LoginPageModule,
    HomePageModule,
    IonicModule.forRoot(MainApp, {
      preloadModules: true
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MainApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    AlertService,
    ToastService,
    CameraProvider,
    FileProvider,
    Camera,
    UserService,
    LoanService,
  ]
})
export class AppModule {}
