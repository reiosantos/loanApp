import { Component, ViewChild } from '@angular/core';
import {App, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

import {AuthProvider} from "../providers/auth/auth";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {ProfilePage} from "../pages/profile/profile";
import {LoanStatusPage} from "../pages/loan-status/loan-status";
import {NewLoanPage} from "../pages/new-loan/new-loan";
import {CreditorsPage} from "../pages/creditors/creditors";
import {environment} from "../environments/environment";


@Component({
  templateUrl: 'app.html'
})
export class MainApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public app: App,
    public auth: AuthProvider,
    ) {
    this.initializeApp();
    this.prepareAuth();

    if (localStorage.getItem(environment.currentUser) !== null){
      this.rootPage = HomePage;
    }

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: "home"},
      { title: 'Creditors', component: CreditorsPage, icon: "people" },
      { title: 'New Loan', component: NewLoanPage, icon: "contract" },
      { title: 'Loan Status', component: LoanStatusPage, icon: "book" },
      { title: 'Profile', component: ProfilePage, icon: "person" },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      if(this.splashScreen){
        setTimeout(()=> {
          this.splashScreen.hide();
        }, 100);
      }
      this.splashScreen.hide();
    });
  }

  prepareAuth () {

    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else {
        this.rootPage = HomePage;
        unsubscribe();
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logOut() {
    this.auth.logOut();
  }

  exit() {
    this.platform.exitApp();
  }
}
